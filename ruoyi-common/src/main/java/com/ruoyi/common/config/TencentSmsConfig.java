package com.ruoyi.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Describe: 腾讯云短信配置
 * 发送短信接入文档：https://cloud.tencent.com/document/api/382/55981
 * 使用SDK调用
 * 参考使用腾讯云的API Explorer
 * @Author:
 * @Date: 2022/4/11 9:05
 */

@Component
@ConfigurationProperties(prefix = "sms.tencent")
public class TencentSmsConfig {
    /**
     * API相关
     */
    private static String URL ="sms.tencentcloudapi.com";
    private static final String REGION = "ap-guangzhou";
    /**
     * 账号相关
     */
    @Value("${sms.tencent.secretId}")
    private String secretId;
    @Value("${sms.tencent.secretKey}")
    private String secretKey;

    @Value("${sms.tencent.sdkAppId}")
    private String sdkAppId;
    @Value("${sms.tencent.templateId}")
    private String templateId;
    @Value("${sms.tencent.signName}")
    private String signName;


//    @Bean
//    public SmsClient smsClient(){
//        // 实例化一个认证对象，入参需要传入腾讯云账户secretId，secretKey,此处还需注意密钥对的保密
//        // 密钥可前往https://console.cloud.tencent.com/cam/capi网站进行获取
//        Credential cred = new Credential(SECRET_ID, SECRET_KEY);
//        // 实例化一个http选项，可选的，没有特殊需求可以跳过
//        HttpProfile httpProfile = new HttpProfile();
//        httpProfile.setEndpoint(URL);
//        // 实例化一个client选项，可选的，没有特殊需求可以跳过
//        ClientProfile clientProfile = new ClientProfile();
//        clientProfile.setHttpProfile(httpProfile);
//        //实例化 SMS 的 client 对象
//        return new SmsClient(cred, REGION, clientProfile);
//    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getSdkAppId() {
        return sdkAppId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public String getSignName() {
        return signName;
    }
}
